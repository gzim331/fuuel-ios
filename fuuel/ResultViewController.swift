//
//  ResultViewController.swift
//  fuuel
//
//  Created by michal on 13/08/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

    @IBOutlet weak var priceResult: UILabel!
    
    var route: Float = 0
    var price: Float = 0
    var conbustion: Float = 0
    var weight: Float = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        countPrice()
    }
    
    func countPrice() {
        let weightTmp = (0.6*weight)/100
        let result = (((conbustion+weightTmp)*route)/100)*price
        
        priceResult.text = "\(ceil(result))"
    }
}
