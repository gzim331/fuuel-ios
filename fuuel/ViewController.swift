//
//  ViewController.swift
//  fuuel
//
//  Created by michal on 13/08/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var routeText: UITextField!
    @IBOutlet weak var priceText: UITextField!
    @IBOutlet weak var conbustionText: UITextField!
    @IBOutlet weak var weightText: UITextField!
    
    @IBOutlet weak var errorText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
//        routeText.resignFirstResponder()
//        priceText.resignFirstResponder()
//        conbustionText.resignFirstResponder()
//        weightText.resignFirstResponder()
    }
    
    @IBAction func countPressed(_ sender: Any) {
        if validation() == true {
            performSegue(withIdentifier: "result", sender: self)
        } else {
            errorText.text = "Uzupełnij dane"
        }
    }
    
    func validation() -> Bool {
        let route = Float(routeText.text!)
        let price = Float(priceText.text!)
        let conbustion = Float(conbustionText.text!)
        
        if route == nil || price == nil || conbustion == nil {
            return false
        } else {
            return true
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "result" {
            let resultVC = segue.destination as! ResultViewController
            
            resultVC.route = Float(routeText.text!)!
            resultVC.price = Float(priceText.text!)!
            resultVC.conbustion = Float(conbustionText.text!)!
            let weight = Float(weightText.text!)
            
            if weight == nil {
                resultVC.weight = 0
            } else {
                resultVC.weight = weight!
            }
        }
    }
    
}

